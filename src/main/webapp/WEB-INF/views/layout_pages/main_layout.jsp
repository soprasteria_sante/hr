<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css"/>

<title>
	<t:insertAttribute name="title"/>
</title>
</head>
<body>


<div>
		<t:insertAttribute name="header"/>
	</div>
	<div style="padding-top:-15px;padding-bottom:0px;">
		<div class='content'>
			<t:insertAttribute name="body"/>
		</div>
	</div>
	<div style="width:100%;">
		<t:insertAttribute name="footer"/>
	</div>

</body>

</html>