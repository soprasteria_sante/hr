package com.sopra.humanresource.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class FinancialDetailType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5816589747241888095L;
	
}
