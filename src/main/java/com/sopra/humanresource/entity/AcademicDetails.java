package com.sopra.humanresource.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class AcademicDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5088754987965573782L;
	private String id;
	private String name;
	private Date start;
	private Date finish;
	private boolean persuing;
	private UniversityDetails universityDetails;
	private CollegeDetails collegeDetails;
	private boolean highest;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getFinish() {
		return finish;
	}

	public void setFinish(Date finish) {
		this.finish = finish;
	}

	public boolean isPersuing() {
		return persuing;
	}

	public void setPersuing(boolean persuing) {
		this.persuing = persuing;
	}

	public UniversityDetails getUniversityDetails() {
		return universityDetails;
	}

	public void setUniversityDetails(UniversityDetails universityDetails) {
		this.universityDetails = universityDetails;
	}

	public CollegeDetails getCollegeDetails() {
		return collegeDetails;
	}

	public void setCollegeDetails(CollegeDetails collegeDetails) {
		this.collegeDetails = collegeDetails;
	}

	public boolean isHighest() {
		return highest;
	}

	public void setHighest(boolean highest) {
		this.highest = highest;
	}

}
