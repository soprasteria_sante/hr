package com.sopra.humanresource.entity;

import java.io.Serializable;
/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class AssetType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6845501757364406991L;
	private String id;
	private String name;
	private Date addedOn;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	
	
}
