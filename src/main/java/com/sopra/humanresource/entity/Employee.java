package com.sopra.humanresource.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class Employee implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5959170873824099148L;
	private String id;
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	private Designation designation;
	private Address homeaAdress;
	private Department department;
	private Address officeAddress;
	private Set<AcademicDetails> academicDetails;
	private Set<EmploymentDetails> employmentDetails;
	private Set<FinancialDetails> financialDetails;
	private List<Asset> assets;
	private Seat seat;
	private String email;
	private boolean active;
	private Date joinedOn;
	private Date leftOn;
	private boolean inNoticePeriod;
	private boolean inProbation;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Designation getDesignation() {
		return designation;
	}
	public void setDesignation(Designation designation) {
		this.designation = designation;
	}
	public Address getHomeaAdress() {
		return homeaAdress;
	}
	public void setHomeaAdress(Address homeaAdress) {
		this.homeaAdress = homeaAdress;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public Address getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(Address officeAddress) {
		this.officeAddress = officeAddress;
	}
	
	public Set<AcademicDetails> getAcademicDetails() {
		return academicDetails;
	}
	public void setAcademicDetails(Set<AcademicDetails> academicDetails) {
		this.academicDetails = academicDetails;
	}
	public Set<EmploymentDetails> getEmploymentDetails() {
		return employmentDetails;
	}
	public void setEmploymentDetails(Set<EmploymentDetails> employmentDetails) {
		this.employmentDetails = employmentDetails;
	}
	public Set<FinancialDetails> getFinancialDetails() {
		return financialDetails;
	}
	public void setFinancialDetails(Set<FinancialDetails> financialDetails) {
		this.financialDetails = financialDetails;
	}
	public List<Asset> getAssets() {
		return assets;
	}
	public void setAssets(List<Asset> assets) {
		this.assets = assets;
	}
	public Seat getSeat() {
		return seat;
	}
	public void setSeat(Seat seat) {
		this.seat = seat;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getJoinedOn() {
		return joinedOn;
	}
	public void setJoinedOn(Date joinedOn) {
		this.joinedOn = joinedOn;
	}
	public Date getLeftOn() {
		return leftOn;
	}
	public void setLeftOn(Date leftOn) {
		this.leftOn = leftOn;
	}
	public boolean isInNoticePeriod() {
		return inNoticePeriod;
	}
	public void setInNoticePeriod(boolean inNoticePeriod) {
		this.inNoticePeriod = inNoticePeriod;
	}
	public boolean isInProbation() {
		return inProbation;
	}
	public void setInProbation(boolean inProbation) {
		this.inProbation = inProbation;
	}
	
	
	
	
}
