package com.sopra.humanresource.entity;

import java.io.Serializable;
/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Asset implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8324024059893966438L;
	private String id;
	private AssetType type;
	private boolean free;
	private Employee allocatedTo;
	private Date purchasedOn;
	private Date validTill;
	private String serialId;
	private Company company;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public AssetType getType() {
		return type;
	}
	public void setType(AssetType type) {
		this.type = type;
	}
	public boolean isFree() {
		return free;
	}
	public void setFree(boolean free) {
		this.free = free;
	}
	public Employee getAllocatedTo() {
		return allocatedTo;
	}
	public void setAllocatedTo(Employee allocatedTo) {
		this.allocatedTo = allocatedTo;
	}
	public Date getPurchasedOn() {
		return purchasedOn;
	}
	public void setPurchasedOn(Date purchasedOn) {
		this.purchasedOn = purchasedOn;
	}
	public Date getValidTill() {
		return validTill;
	}
	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}
	public String getSerialId() {
		return serialId;
	}
	public void setSerialId(String serialId) {
		this.serialId = serialId;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	
	
	
}
