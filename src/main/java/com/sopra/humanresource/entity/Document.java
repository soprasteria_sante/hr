package com.sopra.humanresource.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class Document implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5770911119313979599L;
	private String id;
	private String name;
	private DocumentType documentType;
	private boolean classified;
	private boolean deleted;
	private boolean publicallyShared;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public DocumentType getDocumentType() {
		return documentType;
	}
	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}
	public boolean isClassified() {
		return classified;
	}
	public void setClassified(boolean classified) {
		this.classified = classified;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public boolean isPublicallyShared() {
		return publicallyShared;
	}
	public void setPublicallyShared(boolean publicallyShared) {
		this.publicallyShared = publicallyShared;
	}
	
	
}
