package com.sopra.humanresource.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class CollegeDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3299701059021783488L;
	private String id;
	private String name;
	private Address address;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
}
