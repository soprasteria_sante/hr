package com.sopra.humanresource.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class Seat implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4175249197200495275L;
	private String id;
	private Address address;
	private Company company;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	
	
}
