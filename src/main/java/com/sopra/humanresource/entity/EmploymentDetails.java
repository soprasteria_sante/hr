package com.sopra.humanresource.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class EmploymentDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8964443498722281151L;
	private String id;
	private Company company;
	private Date start;
	private Date end;
	private String causeOfLeave;
	private Set<Document> documents;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public String getCauseOfLeave() {
		return causeOfLeave;
	}
	public void setCauseOfLeave(String causeOfLeave) {
		this.causeOfLeave = causeOfLeave;
	}
	public Set<Document> getDocuments() {
		return documents;
	}
	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}
	
	
}
