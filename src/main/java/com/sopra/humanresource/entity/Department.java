package com.sopra.humanresource.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class Department implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1206090064943341511L;
	private String id;
	private String name;
	private Employee hod;
	private String email;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Employee getHod() {
		return hod;
	}
	public void setHod(Employee hod) {
		this.hod = hod;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
