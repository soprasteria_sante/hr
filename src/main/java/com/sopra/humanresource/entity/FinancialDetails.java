package com.sopra.humanresource.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
@Entity
@Table
public class FinancialDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8787070162838143783L;
	private String id;
	private String name;
	private FinancialDetailType financialDetailType;
	private String finyear;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public FinancialDetailType getFinancialDetailType() {
		return financialDetailType;
	}
	public void setFinancialDetailType(FinancialDetailType financialDetailType) {
		this.financialDetailType = financialDetailType;
	}
	public String getFinyear() {
		return finyear;
	}
	public void setFinyear(String finyear) {
		this.finyear = finyear;
	}
	
	
	
	
}
