package com.sopra.humanresource.entity;


import java.io.Serializable;
/**
 * @author Nikhil(mailto:nikhil.j2se@gmail.com)
 *
 */
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Company implements Serializable{
	private String id;
	private String name;
	private Address address;
	private String email;
	private Date addedOn;
	private boolean active;
	private boolean deleted;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	
}
