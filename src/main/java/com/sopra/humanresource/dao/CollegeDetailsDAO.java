/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.CollegeDetails;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 7:56:30 PM
 */
public interface CollegeDetailsDAO {
	public void save(CollegeDetails collegeDetails);
	public List<CollegeDetails> getListByEmployee(Employee employee);
	public List<CollegeDetails> getListByAddress(Address address);
}
