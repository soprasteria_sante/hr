/**
 * 
 */
package com.sopra.humanresource.dao;

import com.sopra.humanresource.entity.Seat;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 8:13:33 PM
 */
public interface SeatDAO {
	public void save(Seat seat);
}
