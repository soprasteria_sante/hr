/**
 * 
 */
package com.sopra.humanresource.dao;

import com.sopra.humanresource.entity.AssetType;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 7:55:00 PM
 */
public interface AssetTypeDAO {
	public void save(AssetType assetType);
	
	
}
