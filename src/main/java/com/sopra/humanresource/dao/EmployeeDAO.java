/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.Asset;
import com.sopra.humanresource.entity.Department;
import com.sopra.humanresource.entity.Designation;
import com.sopra.humanresource.entity.Employee;
import com.sopra.humanresource.entity.Seat;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 8:03:57 PM
 */
public interface EmployeeDAO {
	public void save(Employee Employee);
	public List<Employee> getListByAddress(Address address);
	public List<Employee> getListByDesignation(Designation designation);
	public List<Employee> getListByDepartment(Department department);
	public Employee getBySeat(Seat seat);
	public Employee getByAsset(Asset asset);
	
}
