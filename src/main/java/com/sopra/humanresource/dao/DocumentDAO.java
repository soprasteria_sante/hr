/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.Document;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 8:02:00 PM
 */
public interface DocumentDAO {
	public void save(Document document);
	public List<Document> getListByEmployee(Employee employee);
}
