/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.Employee;
import com.sopra.humanresource.entity.UniversityDetails;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 8:14:55 PM
 */
public interface UniversityDetailsDAO {
	public void save(UniversityDetails universityDetails);
	public List<UniversityDetails> GetListByEmloyee(Employee employee);
}
