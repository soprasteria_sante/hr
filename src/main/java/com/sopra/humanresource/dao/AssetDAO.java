/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.Asset;
import com.sopra.humanresource.entity.Company;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 7:51:32 PM
 */
public interface AssetDAO {
	public void save(Asset asset);
	public List<Asset> getListByEmployee(Employee employee);
	public List<Asset> getListByCompany(Company company);
	public List<Asset> getListByAddress(Address address);
}
