/**
 * 
 */
package com.sopra.humanresource.dao;

import com.sopra.humanresource.entity.Designation;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 8:00:41 PM
 */
public interface DesignationDAO {
	public void save(Designation designation);
	public Designation getByEmployee(Employee employee);
	
}
