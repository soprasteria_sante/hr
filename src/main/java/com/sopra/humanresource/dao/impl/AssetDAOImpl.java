/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.AddressDAO;
import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 11:57:01 AM
 */

@Repository
@Transactional
public class AssetDAOImpl implements AddressDAO {

	@Autowired
	private SessionFactory factory;

	private Session getSession() {
		return this.factory.getCurrentSession();
	}

	
	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.AddressDAO#save(com.sopra.humanresource.entity.Address)
	 */
	@Override
	public void save(Address address) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.AddressDAO#getListByEmployee(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public List<Address> getListByEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
