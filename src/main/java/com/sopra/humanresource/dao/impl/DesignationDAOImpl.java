/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.DesignationDAO;
import com.sopra.humanresource.entity.Designation;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:02:19 PM
 */
@Repository
@Transactional
public class DesignationDAOImpl implements DesignationDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.DesignationDAO#save(com.sopra.humanresource.entity.Designation)
	 */
	@Override
	public void save(Designation designation) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.DesignationDAO#getByEmployee(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public Designation getByEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
