/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.CompanyDAO;
import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.Company;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:00:36 PM
 */
@Repository
@Transactional
public class CompanyDAOImpl implements CompanyDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.CompanyDAO#save(com.sopra.humanresource.entity.Company)
	 */
	@Override
	public void save(Company company) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.CompanyDAO#getListByEmployee(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public List<Company> getListByEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.CompanyDAO#getListByAddress(com.sopra.humanresource.entity.Address)
	 */
	@Override
	public List<Company> getListByAddress(Address address) {
		// TODO Auto-generated method stub
		return null;
	}

}
