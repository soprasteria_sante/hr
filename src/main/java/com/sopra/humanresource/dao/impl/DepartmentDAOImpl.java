/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.DepartmentDAO;
import com.sopra.humanresource.entity.Department;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:01:35 PM
 */
@Repository
@Transactional
public class DepartmentDAOImpl implements DepartmentDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.DepartmentDAO#save(com.sopra.humanresource.entity.Department)
	 */
	@Override
	public void save(Department department) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.DepartmentDAO#getListByEmployee(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public List<Department> getListByEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
