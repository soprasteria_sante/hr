/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.AcademicDetailsDAO;
import com.sopra.humanresource.entity.AcademicDetails;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com) 13-Apr-2019 8:25:47 PM
 */
@Repository
@Transactional
public class AcademicDetailsDAOImpl implements AcademicDetailsDAO {

	@Autowired
	private SessionFactory factory;

	private Session getSession() {
		return this.factory.getCurrentSession();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sopra.humanresource.dao.AcademicDetailsDAO#save(com.sopra.humanresource.
	 * entity.AcademicDetails)
	 */
	@Override
	public void save(AcademicDetails academicDetails) {
		this.getSession().save(academicDetails);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sopra.humanresource.dao.AcademicDetailsDAO#getListByEmployee(com.sopra.
	 * humanresource.entity.Employee)
	 */
	@Override
	public List<AcademicDetails> getListByEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
