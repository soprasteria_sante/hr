/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.FinancialDetailsDAO;
import com.sopra.humanresource.entity.Employee;
import com.sopra.humanresource.entity.FinancialDetails;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:06:35 PM
 */
@Repository
@Transactional
public class FinancialDetailsDAOImpl implements FinancialDetailsDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.FinancialDetailsDAO#save(com.sopra.humanresource.entity.FinancialDetails)
	 */
	@Override
	public void save(FinancialDetails financialDetail) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.FinancialDetailsDAO#getByEmployee(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public FinancialDetails getByEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
