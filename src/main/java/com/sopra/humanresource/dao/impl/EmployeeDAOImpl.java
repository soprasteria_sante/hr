/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.EmployeeDAO;
import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.Asset;
import com.sopra.humanresource.entity.Department;
import com.sopra.humanresource.entity.Designation;
import com.sopra.humanresource.entity.Employee;
import com.sopra.humanresource.entity.Seat;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:04:55 PM
 */
@Repository
@Transactional
public class EmployeeDAOImpl implements EmployeeDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.EmployeeDAO#save(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public void save(Employee Employee) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.EmployeeDAO#getListByAddress(com.sopra.humanresource.entity.Address)
	 */
	@Override
	public List<Employee> getListByAddress(Address address) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.EmployeeDAO#getListByDesignation(com.sopra.humanresource.entity.Designation)
	 */
	@Override
	public List<Employee> getListByDesignation(Designation designation) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.EmployeeDAO#getListByDepartment(com.sopra.humanresource.entity.Department)
	 */
	@Override
	public List<Employee> getListByDepartment(Department department) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.EmployeeDAO#getBySeat(com.sopra.humanresource.entity.Seat)
	 */
	@Override
	public Employee getBySeat(Seat seat) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.EmployeeDAO#getByAsset(com.sopra.humanresource.entity.Asset)
	 */
	@Override
	public Employee getByAsset(Asset asset) {
		// TODO Auto-generated method stub
		return null;
	}

}
