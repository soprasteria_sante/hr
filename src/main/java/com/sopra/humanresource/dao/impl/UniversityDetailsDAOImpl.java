/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.UniversityDetailsDAO;
import com.sopra.humanresource.entity.Employee;
import com.sopra.humanresource.entity.UniversityDetails;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:10:05 PM
 */
@Repository
@Transactional
public class UniversityDetailsDAOImpl implements UniversityDetailsDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.UniversityDetailsDAO#save(com.sopra.humanresource.entity.UniversityDetails)
	 */
	@Override
	public void save(UniversityDetails universityDetails) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.UniversityDetailsDAO#GetListByEmloyee(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public List<UniversityDetails> GetListByEmloyee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
