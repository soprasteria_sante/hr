/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.SeatDAO;
import com.sopra.humanresource.entity.Seat;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:09:01 PM
 */
@Repository
@Transactional
public class SeatDAOImpl implements SeatDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.SeatDAO#save(com.sopra.humanresource.entity.Seat)
	 */
	@Override
	public void save(Seat seat) {
		// TODO Auto-generated method stub

	}

}
