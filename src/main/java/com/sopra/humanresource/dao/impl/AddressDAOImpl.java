/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.AddressDAO;
import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com) 13-Apr-2019 8:28:20 PM
 */
@Repository
@Transactional
public class AddressDAOImpl implements AddressDAO {

	@Autowired
	private SessionFactory factory;

	private Session getSession() {
		return this.factory.getCurrentSession();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sopra.humanresource.dao.AddressDAO#save(com.sopra.humanresource.entity.
	 * Address)
	 */
	@Override
	public void save(Address address) {
		this.getSession().save(address);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sopra.humanresource.dao.AddressDAO#getListByEmployee(com.sopra.
	 * humanresource.entity.Employee)
	 */
	@Override
	public List<Address> getListByEmployee(Employee employee) {
		Criteria c = this.getSession().createCriteria(Address.class);
		c.add(Restrictions.eq("Employee", employee));
		return c.list();
	}

}
