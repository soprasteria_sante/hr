/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.DocumentDAO;
import com.sopra.humanresource.entity.Document;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:02:51 PM
 */
@Repository
@Transactional
public class DocumentDAOImpl implements DocumentDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.DocumentDAO#save(com.sopra.humanresource.entity.Document)
	 */
	@Override
	public void save(Document document) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.DocumentDAO#getListByEmployee(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public List<Document> getListByEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
