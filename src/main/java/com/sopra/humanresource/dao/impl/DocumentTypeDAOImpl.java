/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.DocumentTypeDAO;
import com.sopra.humanresource.entity.DocumentType;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:03:22 PM
 */
@Repository
@Transactional
public class DocumentTypeDAOImpl implements DocumentTypeDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.DocumentTypeDAO#save(com.sopra.humanresource.entity.DocumentType)
	 */
	@Override
	public void save(DocumentType documentType) {
		// TODO Auto-generated method stub
		
	}

}
