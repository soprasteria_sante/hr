/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.FinancialDetailTypeDAO;
import com.sopra.humanresource.entity.FinancialDetailType;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:07:30 PM
 */
@Repository
@Transactional
public class FinancialDetailTypeDAOImpl implements FinancialDetailTypeDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.FinancialDetailTypeDAO#save(com.sopra.humanresource.entity.FinancialDetailType)
	 */
	@Override
	public void save(FinancialDetailType financialDetailType) {
		// TODO Auto-generated method stub

	}

}
