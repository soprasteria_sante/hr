/**
 * 
 */
package com.sopra.humanresource.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sopra.humanresource.dao.EmploymentDetailDAO;
import com.sopra.humanresource.entity.Employee;
import com.sopra.humanresource.entity.EmploymentDetails;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 15-Apr-2019 12:05:25 PM
 */
@Repository
@Transactional
public class EmploymentDetailDAOImpl implements EmploymentDetailDAO {

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.EmploymentDetailDAO#save(com.sopra.humanresource.entity.EmploymentDetails)
	 */
	@Override
	public void save(EmploymentDetails employmentDetail) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sopra.humanresource.dao.EmploymentDetailDAO#getByEmployee(com.sopra.humanresource.entity.Employee)
	 */
	@Override
	public EmploymentDetails getByEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
