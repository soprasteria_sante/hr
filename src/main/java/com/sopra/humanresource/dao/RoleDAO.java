/**
 * 
 */
package com.sopra.humanresource.dao;

import com.sopra.humanresource.entity.Role;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 8:12:25 PM
 */
public interface RoleDAO {
	public void save(Role role);
}
