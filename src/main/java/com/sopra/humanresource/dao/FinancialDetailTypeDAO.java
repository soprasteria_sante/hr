/**
 * 
 */
package com.sopra.humanresource.dao;

import com.sopra.humanresource.entity.FinancialDetailType;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 8:03:12 PM
 */
public interface FinancialDetailTypeDAO {
	public void save(FinancialDetailType financialDetailType);
	
}
