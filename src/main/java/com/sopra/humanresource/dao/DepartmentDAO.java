/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.Department;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 7:59:34 PM
 */
public interface DepartmentDAO {
	public void save(Department department);
	public List<Department> getListByEmployee(Employee employee);
	
}
