/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.AcademicDetails;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 7:42:16 PM
 */
public interface AcademicDetailsDAO {
	public void save(AcademicDetails academicDetails);
	public List<AcademicDetails> getListByEmployee(Employee employee);
}
