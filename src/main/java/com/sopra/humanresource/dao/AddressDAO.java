/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 7:49:31 PM
 */
public interface AddressDAO {
	public void save(Address address);
	public List<Address> getListByEmployee(Employee employee);
}
