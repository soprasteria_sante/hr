/**
 * 
 */
package com.sopra.humanresource.dao;

import com.sopra.humanresource.entity.Employee;
import com.sopra.humanresource.entity.FinancialDetails;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 8:07:54 PM
 */
public interface FinancialDetailsDAO {
	public void save(FinancialDetails financialDetail);
	public FinancialDetails getByEmployee(Employee employee);
	
}
