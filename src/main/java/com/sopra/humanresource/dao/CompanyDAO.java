/**
 * 
 */
package com.sopra.humanresource.dao;

import java.util.List;

import com.sopra.humanresource.entity.Address;
import com.sopra.humanresource.entity.Company;
import com.sopra.humanresource.entity.Employee;

/**
 * @author Nilhil(mailto:nikhil.j2se@gmail.com)
 * 13-Apr-2019 7:57:56 PM
 */
public interface CompanyDAO {
	public void save(Company company);
	public List<Company> getListByEmployee(Employee employee);
	public List<Company> getListByAddress(Address address);
}
